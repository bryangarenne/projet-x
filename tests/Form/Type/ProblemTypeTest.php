<?php
namespace App\Tests;



use App\Form\ProblemType;
use Symfony\Component\Form\Test\TypeTestCase;


class ProblemTypeTest extends TypeTestCase
{
    public function testSubmit()
    {
        $data= [
            "title"=>"test",
            "description"=>"test",            
        ];

        $type = new ProblemType();
        $form = $this->factory->create(ProblemType::class);
        $form->submit($data);

        dump($form);
        $this->assertTrue($form->isSynchronized());
    }
}