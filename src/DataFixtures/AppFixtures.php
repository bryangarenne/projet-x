<?php

namespace App\DataFixtures;

use App\Entity\Tag;
use App\Entity\User;
use App\Entity\Problem;
use App\Entity\Solution;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    public function __construct(UserPasswordEncoderInterface $pswEncoder) {

      $this->pswEncoder = $pswEncoder; 

    }
    public function load(ObjectManager $manager)
    {

          
        $userG;
          foreach ($this->getUserData() as [$fName, $lName, $email, $password, $roles]) {
    
          $user = new User(); 
          $user->setfirstName($fName);
          $user->setlastName($lName);
          $user->setEmail($email); 
          $user->setPassword($this->pswEncoder->encodePassword($user, $password));
          $user->setRoles($roles);
          $manager->persist($user);
          $userG = $user;
      }
    
          $tag = new Tag(); 
          $tag->setName("Finances"); 

          $problem = new Problem();
          $problem->setTitle("Aled");
          $problem->setDescription("super description de dingo malade");
          $problem->setUser($userG);
          $problem->setTag($tag);
          $manager->persist($problem);
          
          $solution = new Solution();
          $solution->setName("Solution Test");
          $solution->setDescription("Description de solution dadadidadadon");
          $solution->setProblem($problem);
          $solution->setUser($userG);
          $manager->persist($solution);
        

          $tag1 = new Tag();
          $tag1->setName("Retraite");

          $problem1 = new Problem();
          $problem1->setTitle("Aled");
          $problem1->setDescription("super description de dingo malade");
          $problem1->setUser($userG);
          $problem1->setTag($tag1);
          $manager->persist($problem1);


          $solution1 = new Solution();
          $solution1->setName("Solution Test");
          $solution1->setDescription("Description de solution dadadidadadon");
          $solution1->setProblem($problem1);
          $solution1->setUser($userG);
          $manager->persist($solution1);

          $tag2 = new Tag(); 
          $tag2->setName("Social"); 

          $problem2 = new Problem();
          $problem2->setTitle("Aled");
          $problem2->setDescription("super description de dingo malade");
          $problem2->setUser($userG);
          $problem2->setTag($tag2);
          $manager->persist($problem2);

          $solution2 = new Solution();
          $solution2->setName("Solution Test");
          $solution2->setDescription("Description de solution dadadidadadon");
          $solution2->setProblem($problem2);
          $solution2->setUser($userG);
          $manager->persist($solution2);

          $tag3 = new Tag(); 
          $tag3->setName("Santé"); 

          $problem3 = new Problem();
          $problem3->setTitle("Aled");
          $problem3->setDescription("super description de dingo malade");
          $problem3->setUser($userG);
          $problem3->setTag($tag3);
          $manager->persist($problem3);

          $solution3 = new Solution();
          $solution3->setName("Solution Test");
          $solution3->setDescription("Description de solution dadadidadadon");
          $solution3->setProblem($problem3);
          $solution3->setUser($userG);
          $manager->persist($solution3);

          $tag4 = new Tag(); 
          $tag4->setName("Économie"); 

          $problem4 = new Problem();
          $problem4->setTitle("Aled");
          $problem4->setDescription("super description de dingo malade");
          $problem4->setUser($userG);
          $problem4->setTag($tag4);
          $manager->persist($problem4);

          $solution4 = new Solution();
          $solution4->setName("Solution Test");
          $solution4->setDescription("Description de solution dadadidadadon");
          $solution4->setProblem($problem4);
          $solution4->setUser($userG);
          $manager->persist($solution4);

          $manager->persist($tag);
          $manager->persist($tag1);
          $manager->persist($tag2);
          $manager->persist($tag3);
          $manager->persist($tag4);
      
      $manager->flush();
    }


    private function getUserData(): array

    {
      return 
      [
          ['Ian','Chatenet','Ian@chatenet.com', 'ouais', ['ROLE_ADMIN']],
          ['Brayan','Zbra','Brayan@brayan.com', 'brayan123', ['ROLE_USER']],
          ['Test','mail','test@mail.lol', 'banana', ['ROLE_ADMIN']]
      ];
    }



}
