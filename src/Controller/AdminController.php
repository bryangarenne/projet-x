<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(SerializerInterface $serializer)
    {
        $users = $this->getDoctrine()
        ->getRepository(User::class)
        ->findAll();
            
        $jsonContent=$serializer->serialize($users,"json",[AbstractNormalizer::ATTRIBUTES => ['email','id','roles']]);

        
        return $this->render('admin/index.html.twig', [
            'users'=>$jsonContent,
        ]);
    }
}
