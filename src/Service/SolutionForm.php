<?php 

namespace App\Service;

use App\Entity\Problem;
use App\Entity\Solution;
use App\Form\SolutionType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\Form;
use Psr\Container\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class SolutionForm  extends AbstractType{

  private $formSolution; 
  private $em;
  protected $container;
  private $security;


  public function __construct(ContainerInterface $container, EntityManagerInterface $em, Security $security) {

      $this->formSolution = SolutionType::class;
      $this->container = $container;
      $this->em = $em;
      $this->security = $security; 
  }


  public function createFormSolution(Request $request, Problem $problem ) {
  
      $solution = new Solution();  

      $form = $this->createForm($this->formSolution, $solution);
      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->security->getUser();
            $solution->setUser($user);
            $solution->setProblem($problem);
            $this->em->persist($solution);
            $this->em->flush();
          
        }
        return $form; 
    
  }

public function createForm($type, $data = null, array $options = array())
{
    return $this->container->get('form.factory')->create($type, $data, $options);
}




}